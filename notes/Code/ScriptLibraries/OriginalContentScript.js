var revisionMapURL;

var revisionMap;
var linkList = new Object();
var windowLink;
var qo;

function initOrigMode(){
	windowLink = window.location.href;
	var q = windowLink.substring(windowLink.indexOf("#") + 1, windowLink.length);
  	 qo = dojo.queryToObject(q);
  	
	if(qo.action){
		revisionMapURL = "originalcontent?ReadViewEntries&outputformat=json&count=-1";
	}else{
		revisionMapURL = "../originalcontent?ReadViewEntries&outputformat=json&count=-1";
	}
 	if(qo.mode === "original"){
  			getRevisionMap();
  		}
}

function changeContentLinks(){

var oMode = dojo.byId("original_mode");
var cMode = dojo.byId("current_mode");
var currLink = windowLink.split("?openDocument&mode");
// determine if this is the original version of the article
var temp = getResTitle(currLink[0].toLowerCase());
var test = linkList[temp];
 if(test){
        	//links[i].href = "../page.xsp?documentId=" + temp + "&action=openDocument";
        	omode.href = "../page.xsp?documentId=" + temp + "&action=openDocument";
        	
        }

}
    
function buildOrigLinkList(){
	
    for(var i = 0; i < revisionMap.viewentry.length; i++){
    	linkList[revisionMap.viewentry[i].entrydata[2].text[0]] = revisionMap.viewentry[i]["@unid"];    
	}
}

function scrubTopicLinks(link){
    var links;
    if(link){
    	links = new Array(link);
    }else{
    	links = dojo.query("#wikiDoc a")
    }
    for(var i=0; i<links.length; i++){
    	var resTitle = getResTitle(links[i]);
        //var originalRevLink = revisionMap[resTitle];  
        var originalRevLink = linkList[resTitle];
        if(originalRevLink){
        	return originalRevLink;
        }
     }
 }
 
 function changeLinks(){
 	
 	var hostName = window.location.hostname
 	 var links = dojo.query("#wikiDoc a")
    for(var i=0; i<links.length; i++){
    	var oldLink = links[i].href;
    	var test = oldLink.search("page.xsp");
    	// look for the domain of the url
    	var urlDom = oldLink.split("/");
    	if (hostName === urlDom[2]){
    		if(test === -1){
    			links[i].href = oldLink + "?OpenDocument&mode=original";
    		}else{
    			links[i].href = oldLink + "&mode=original";
    		}
    	}
    }
 }

 function getResTitle(articleLink){
 	var resTitle;
    if(articleLink){
    	//resTitle = articleLink.id;
    	if(articleLink.href){
    	    var lastSlash = articleLink.href.lastIndexOf("/");
            resTitle = articleLink.href.substring(lastSlash + 1);
        }else{
           try{
        	  var lastSlash = articleLink.lastIndexOf("/");
              resTitle = articleLink.substring(lastSlash + 1);
           }catch(err){
           }
        }    
     }
        return resTitle;
 }

function getRevisionMap(link){
	var xhrArgs = {
            url: revisionMapURL,
            handleAs: "json",
            load: function(data) {
                 revisionMap = data;
                 buildOrigLinkList();
                 if(link){
                 scrubTopicLinks(link);
                 }else{
                 scrubTopicLinks();
                 changeLinks();
                 }
              //   changeContentLinks();
            },
            error: function(error) {
                alert("unable to obtain revisionMap object: " + http_request.status);
            }
        }

        //Call the asynchronous xhrGet
        var deferred = dojo.xhrGet(xhrArgs);

}

function getTOCLink(link,dnc){
	//check to see if we need to handle this link as a category navigation item
	//or as a product documentation navigation item
	//note: this is needed since the navTree control is being used in multiple places
	// MKS 12/9/2010: If topichead, just return.
	if(link===""){
		return;
	} else {
	if(link.indexOf("lookupName") >= 0){
		//handle as a category navigation
		window.location = "xpViewCategories.xsp?" + link;
	}
	else{		
		//determine if original mode is set
		if(qo && qo.mode && qo.mode === "original"){
			if(revisionMap){
				var newlink = scrubTopicLinks(link);
				if(newlink){
					XSP.showContent(dnc,"pdcontent",{action:'openDocument',documentId:newlink,mode:"original"});
				}
				else{
					XSP.showContent(dnc,"pdcontent",{action:'openDocument',res_title:link,mode:"original"});
				}
			}
		}
		else{
			XSP.showContent(dnc,"pdcontent",{action:'openDocument',res_title:link,mode:"original"});
		}	
	}
	}
}
    
    
  