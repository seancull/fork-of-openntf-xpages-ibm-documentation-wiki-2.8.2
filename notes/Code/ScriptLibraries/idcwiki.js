if (window.location.protocol == "https:") {
var host = window.location.hostname;
var pathname = window.location.pathname;
var search = window.location.search;
var hash = window.location.hash;
var url = "http://" + host + pathname + search + hash;
location.replace(url);
}

function writeStat(pth,page,xhtml,subject){
var docurl=document.URL;
docurl=docurl.split("#")[0];
document.write("<img alt='Statistics' style='position:absolute;left:1px;top:1px;visibility:hidden' width='1' height='1' src='http://"+pth+"/fLog?CreateDocument&t="+docurl+"&r="+document.referrer+"&s="+subject+"&p="+page+"' "+xhtml+">");
}

function goToURL(link)
  {
      
    if(!link=="")
      {
        window.location.href=link
    }
}

function catNavURL(link,type,h){

	var prefix;
	var sURL = window.location.href;
	if(sURL.indexOf("xpViewCategories") >= 0) {
		prefix = "";
	}else if(sURL.indexOf("/dx/") >= 0){
		prefix = "../";	
	}else{
		prefix = h + ".nsf/";
	}

	if(type === "external"){
		window.location.href=link	
	}else if(type === "article" || type === 'category'){
			window.location.href = prefix + link;
	}
	
}
function changeNFSelector(lang,rte,nFlag){
	var nfWidget = dojo.byId('ietran_ui_langselect');
	var rteOpt = dojo.byId(rte);
	
	if(lang === "1" || nFlag === true){
		rteOpt.style.display = "none";
	}else{
		rteOpt.style.display = "";
	}
	
	nfWidget.value = lang;
	translate();
	

	}

function searchTrees(currentTopicTitle, model, articleList, item) {
	
    var id = model.getIdentity(item);
    articleList.push(id);
    if (id == currentTopicTitle) {
        return articleList;
    }
    for (var idx in item.children) {
        var buildmebranch = articleList.slice(0);
        var r = searchTrees(currentTopicTitle, model, buildmebranch, item.children[idx]);
        if (r) {
            return r;
        }
    }
    return undefined;
}

function selectArticleFromTree(tree, currentTopicTitle) {
    var articleList = new Array();
    var result = searchTrees(currentTopicTitle, tree.model, articleList, tree.model.root);
    if (result && result.length > 0) {
       return result;
    }
}

function crumbLink(currentTopicTitle, model,item) {
	var c = ""; 
    var id = model.getIdentity(item);
    var disp = item.display;
    if(disp === undefined){
    	disp = item.name;
    	c = disp + "~" + item.link + "~old";
    }else{
    	c = disp + "~" + item.link;
    }
    
    if (id == currentTopicTitle) {
        return c;
    }
    for (var idx in item.children) {
        var r = crumbLink(currentTopicTitle, model, item.children[idx]);
        if (r) {
            return r;
        }
    }
    return undefined;
}

var anchor = function(node){
	var ancNode = dojo.byId(node);
	var vw = dojo.query(".lotusContent");
	var scrl = dojox.fx.smoothScroll({node:ancNode, win:vw[0],duration:500});
	scrl.play();
}
