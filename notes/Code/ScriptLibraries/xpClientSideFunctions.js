/**
(c) IBM Corp., 2009 Licensed Materials
 */ 

/**
Licensed to OpenNTF under one or more contributor license agreements.

See the NOTICE file distributed with this work for additional information
regarding copyright ownership.  OpenNTF licenses this file
to you under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. 

*/

//Client Side Read Mode Functions

//Add TOC events to onload
dojo.addOnLoad(initxpcsf);

function initxpcsf(){
eventsTOC();
//setWelcomeBox();
var ck = getCookie("resize");
	if(ck !== undefined){
		var lcl = dojo.byId("lotusColLeft");
		//dojo.byId("lotusColLeft").style.width = ck;	
		dojo.style(lcl,"width",ck);
	}
var ck2 = getCookie("resize2");
	if(ck2 !== undefined){
		dojo.query(".accordPane").forEach(function(node, index, arr){
      	//node.style.width = ck2;
      	dojo.style(node,"width",ck2);
      	});			
	}
}


function eventsTOC(){
//Adds hide/show events to toc - SS JS strips these if coded direct
if(document.getElementById("tocHide")!=null){
document.getElementById("tocHide").onclick=toggleTOC;
document.getElementById("tocShow").onclick=toggleTOC;
}}

function toggleTOC(){
if(document.getElementById("tocHidden").style.display=="" || document.getElementById("tocHidden").style.display=="none" ){
document.getElementById("tocHidden").style.display="block";
document.getElementById("tocVisible").style.display="none";
}else{
document.getElementById("tocHidden").style.display="none";
document.getElementById("tocVisible").style.display="block";
}}

function setCookie(name , val, exp){
	dojo.cookie(name,val,{expires:exp, path:"/ldd"});
	//dojo.byId("welcome").style.display="none";
};

function getCookie(name) {
	if(dojo.cookie){
		return dojo.cookie(name);
	}
};

setWelcomeBox = function(){
 var tmp = getCookie();
 if(tmp === "closed"){
 	dojo.byId("welcome").style.display="none";
 }
};

var storeResize = function(col1,col2,evt,ui){

	var ck = getCookie("resize");
	var nav1 = dojo.byId(col1);
	if(arguments.length > 1){
		// check for custom nav
		var nav2 = dojo.byId(col2);
		// if no custom nav check for accordian panes
		if(nav2 === undefined || nav2 === null){
			nav2 = dojo.query(".accordPane");		
		}
	}
	//setCookie("resize",nav1.style.width,365);
	setCookie("resize",dojo.style(nav1,"width") + "px",365);
	
	if(nav2 !== null){
	var navSize = ""
	// get the width of the current panel
	
	var temp;
	if(nav2.length === 1){
		navSize = dojo.style(nav2[0],"width") + "px";
	}else{
		dojo.query(".accordPane[class*='dijitVisible'").forEach(function(node,index,arr){
			navSize = dojo.style(node,"width") + "px";
		});
		 
	}
	setCookie("resize2",navSize,365);
	}
}

