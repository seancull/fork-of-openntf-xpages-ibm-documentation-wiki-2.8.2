/*
(c) IBM Corp., 2009 Licensed Materials
 */

/*
Licensed to OpenNTF under one or more contributor license agreements.

See the NOTICE file distributed with this work for additional information
regarding copyright ownership.  OpenNTF licenses this file
to you under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. 

*/

//Steve Castledine v.04 April 2009

//Global Preferences
var objPreferences=null;

//Strings will come from property files later
STRING_WELCOME_SUBJECT="Welcome";
//STRING_DEFAULT_PAGE_SUBJECT="Enter Page Subject..";
STRING_DEFAULT_PAGE_SUBJECT="";
STRING_DEFAULT_TITLE="XPages Wiki";
STRING_TOC="Table of Contents";
STRING_CATEGORIES_TITLE="Categories";

// init application
function initApplication() {
	//Get global preferences
	//Revisit the scopes - a lot of viewScopes can go into sessionScope for better caching however misbehaving today

	objPreferences=new wikiPreferences;
	objPreferences.init();
	
}

// Wiki Profile/settings Object

wikiPreferences = function() {

// private class members
	db:	NotesDatabase = null;
	viewPref:NotesView = null;
	configView:NotesView = null;
	profileDoc:NotesDocument = null;		
	configDoc:NotesDocument = null;
							
	//-----------------
	this.prototype.init = function() {
	//-----------------
	
		try {

			db = database;
			viewPref=db.getView("vPref");
			profileDoc = viewPref.getFirstDocument();
			configView = db.getView("vConfig");
			configDoc = configView.getFirstDocument();
						
			if(profileDoc == null){
				profileDoc=db.createDocument();
				profileDoc.appendItemValue("Form","fPref");
				profileDoc.computeWithForm(false,false);				
				profileDoc.save(true,true);				
			}
			
			if(profileDoc !== null && configDoc !== null){
				
				var pdDate:NotesDateTime = applicationScope.profileDate;
				//print("PDDATE = " + applicationScope.profileDate);
				var cdDate:NotesDateTime = applicationScope.configDate;
				//print("CDDATE = " + cdDate);
				if(pdDate === null && cdDate === null){
					//print("!!! No dates found in applicationScope !!!")
					applicationScope.profileDate = profileDoc.getLastModified().toJavaDate();
					//print("Set as ProfileDate = " + applicationScope.profileDate);
					applicationScope.configDate = configDoc.getLastModified().toJavaDate();
					//print("Set as ConfigDate = " + applicationScope.configDate);
					objPreferences.processConfig();
				}else{
					var asp:NotesDateTime = session.createDateTime(applicationScope.profileDate);
					//print("ASP = " + asp);
					var asc:NotesDateTime = session.createDateTime(applicationScope.configDate);
					//print("ASC = " + asc);
					
					var pdays:int = asp.timeDifference(profileDoc.getLastModified());
					//print("PDAYS = " + pdays);
					var cdays:int = asc.timeDifference(configDoc.getLastModified());
					//print("CDAYS = " + cdays);
					// now compare dates in application scope to last modified dates to see if updates have been made.
					if(pdays !== 0 || cdays !== 0){
						//print("*** Config doc or profile doc has been modified ***")
						objPreferences.processConfig();
					}else{
						// everything is set and no need to refresh data
						//print("Nothing has been modified... so moving on!!!");
						return;
					}
				
				}
					
				
			}
					
		} catch (e) {
			print (e);
		}
	}
	
	//-------------------------------------------
	this.prototype.processConfig = function(){
	//-------------------------------------------
		//print("^^^ activating processConfig function ^^^^")
		
		// Set Title
		applicationScope.mainTitle=objPreferences.getTitle();
		
		// set wiki url
		applicationScope.wikiURL = objPreferences.getWikiURL();
		
		// Set Site Description -- BW aug 2011
		applicationScope.siteDescription=objPreferences.getSiteDescription();
		
		// Set Category Feature
		applicationScope.useCategories=objPreferences.doCategories();
		
		// Set Revision status
		applicationScope.doArchive=objPreferences.doArchive();
		
		// Set logout redirect
		applicationScope.logoutredirect=objPreferences.getLogoutRedirect();
		
		// Set option to Log Hits/Referrers
		applicationScope.loghitsregerrers = objPreferences.getLogHitsReferrers();
		
		// Set external log location
		applicationScope.externalloglocation = objPreferences.getExternalLogLocation();
		
		// New - Set translation drop down display - bw 1/20/2011 v7.1
		applicationScope.IBMTranslatedDocDisplay = objPreferences.getIBMTranslatedDocDisplay ();	
			
		// Get HTML Option
		applicationScope.htmlallow=objPreferences.getHTMLOption();	
		
		// get the site URL for wiki
		applicationScope.put("wikiurl",objPreferences.getWikiUrl());
		
		// add tab config informaiton into application scope.
		applicationScope.tabconfig = objPreferences.getTabConfig();
		
		// get featured communities - stay connected data
		applicationScope.FCLinks = objPreferences.getFCLinks();
		
		// get footer go elsewhere data
		applicationScope.GELinks = objPreferences.getGELinks();
		
		// get footer help data
		applicationScope.helpLinks = objPreferences.getHelpLinks();
		
		// get footer about data
		applicationScope.aboutLinks = objPreferences.getAboutLinks();
		
		applicationScope.enableDebug = objPreferences.getDebugSetting();
	}

	//-------------------------------------------
	this.prototype.getFCLinks = function(){
	//-------------------------------------------
	// We need to gather "Featured Communities/Stay Connected" links from preferences
	// Loop through link fields max total = 5
		//print("Inside getFCLinks");
		var fcLabel = "";
		var fcLink = "";
		var fcType = "";
		var fcMap = new Array();
		
		
		for(var x = 1; x <= 5; x++){
			var fcItem = {};
			fcLabel = profileDoc.getItemValueString("FCommLabel" + x);
			//print("Label" + x + " = " + fcLabel);
			fcLink = profileDoc.getItemValueString("FCommURL" + x);
			//print("Link"+ x + " = " + fcLink);
			fcType = profileDoc.getItemValueString("FCommType" + x);
			//print("Link"+ x + " = " + geLink);
			if(fcLabel !== "" && fcLink !== ""){
				fcItem.label = fcLabel;
			//	print("fcItem.label = " + fcItem.label);
				fcItem.link = fcLink;
			//	print("fcItem.link = " + fcItem.link);
				fcItem.type = fcType;
			//	print("fcItem.type = " + fcItem.type);
				fcMap.push(fcItem);
			}
			
		}
		
		return fcMap;
	}

	
	
	//-------------------------------------------
	this.prototype.getGELinks = function(){
	//-------------------------------------------
	// We need to gather "Go Elsewhere" links from preferences
	// Loop through link fields max total = 5
		//print("Inside getGELinks");
		var geLabel = "";
		var geLink = "";
		var geMap = new Array();
		
		
		for(var x = 1; x <= 5; x++){
			var geItem = {};
			geLabel = profileDoc.getItemValueString("GElseLabel" + x);
			//print("Label" + x + " = " + geLabel);
			geLink = profileDoc.getItemValueString("GElseURL" + x);
			//print("Link"+ x + " = " + geLink);
			if(geLabel !== "" && geLink !== ""){
				geItem.label = geLabel;
				//print("geItem.label = " + geItem.label);
				geItem.link = geLink;
				//print("geItem.link = " + geItem.link);
				geMap.push(geItem);
			}
			
		}
		
		return geMap;
	}	
	
	

	//-------------------------------------------
	this.prototype.getHelpLinks = function(){
	//-------------------------------------------
	// We need to gather "Go Elsewhere" links from preferences
	// Loop through link fields max total = 5
		//print("Inside gethelpLinks");
		var helpLabel = "";
		var helpLink = "";
		var helpMap = new Array();
		
		
		for(var x = 1; x <= 5; x++){
			var helpItem = {};
			helpLabel = profileDoc.getItemValueString("HelpLabel" + x);
			//print("Label" + x + " = " + geLabel);
			helpLink = profileDoc.getItemValueString("HelpURL" + x);
			//print("Link"+ x + " = " + geLink);
			if(helpLabel !== "" && helpLink !== ""){
				helpItem.label = helpLabel;
			//	print("helpItem.label = " + helpItem.label);
				helpItem.link = helpLink;
			//	print("helpItem.link = " + helpItem.link);
				helpMap.push(helpItem);
			}
			
		}
		
		return helpMap;
	}

	

	//-------------------------------------------
	this.prototype.getAboutLinks = function(){
	//-------------------------------------------
	// We need to gather "Go Elsewhere" links from preferences
	// Loop through link fields max total = 5
		//print("Inside getAboutLinks");
		var aboutLabel = "";
		var aboutLink = "";
		var aboutMap = new Array();
		
		
		for(var x = 1; x <= 5; x++){
			var aboutItem = {};
			aboutLabel = profileDoc.getItemValueString("AboutLabel" + x);
			//print("Label" + x + " = " + aboutLabel);
			aboutLink = profileDoc.getItemValueString("AboutURL" + x);
			//print("Link"+ x + " = " + aboutLink);
			if(aboutLabel !== "" && aboutLink !== ""){
				aboutItem.label = aboutLabel;
			//	print("aboutItem.label = " + aboutItem.label);
				aboutItem.link = aboutLink;
			//	print("aboutItem.link = " + aboutItem.link);
				aboutMap.push(aboutItem);
			}
			
		}
		return aboutMap;
	}
	
	
	//--------------------------------
	this.prototype.getLogoutRedirect = function() {
	//--------------------------------	
	
	try {
		
	return profileDoc.getItemValueString("logout_redirect");
	} catch (e) {
			print (e);			
	}
	
	}
		
	//--------------------------------
	this.prototype.getTitle = function() {
	//--------------------------------	
	
	try {
		
	return profileDoc.getItemValueString("site_title");
	} catch (e) {
			print (e);			
	}
	
	}
	
	//--------------------------------
	this.prototype.getWikiURL = function() {
	//--------------------------------	
	
	try {
		
	return profileDoc.getItemValueString("config_previewserverurl");
	} catch (e) {
			print (e);			
	}
	
	}
	
	//-------------------------------- BW aug 2011
	this.prototype.getSiteDescription = function() {
	//--------------------------------	
	
	try {
		
	return profileDoc.getItemValueString("site_description");
	} catch (e) {
			print (e);			
	}
	
	}
	
	//--------------------------------
	this.prototype.getLogHitsReferrers = function() {
	//--------------------------------	
	
	try {
		
	if(profileDoc.getItemValueString("config_log") == "Yes"){
		return true;
	}else{
		return false;
	}
	} catch (e) {
			print (e);			
	}
	
	}
	
	//--------------------------------
	this.prototype.getLogRSSHits = function() {
	//--------------------------------	
	
	try {
		
	if( profileDoc.getItemValueString("config_logrss") == "Yes"){
		return true;
		}else{
		return false;
		}
	} catch (e) {
			print (e);			
	}
	
	}
	
	//--------------------------------
	this.prototype.getExternalLogLocation = function() {
	//--------------------------------	
	
	try {
		
	return profileDoc.getItemValueString("config_loglocation");
	} catch (e) {
			print (e);			
	}
	
	}
		
	
		//--------------------------------
	this.prototype.getIBMTranslatedDocDisplay = function() {
  		//--------------------------------	
	
	try {
		if(profileDoc.getItemValueString("show_IBM_translated_doc") === "Yes"){
			return true;		
		}else{
			return false;
		}
	
	} catch (e) {
			print (e);			
	}
	
	}		
			
	
	//--------------------------------
	this.prototype.doArchive = function() {
	//--------------------------------	
	
	try {
				
		if(profileDoc.getItemValueString("feature_revisions")=="Y"){
			return true;
		}else{
			return false;
		}
		
	} catch (e) {
			print (e);			
	}}
	
	//--------------------------------
	this.prototype.doCategories = function() {
	//--------------------------------	
	
	try {
				
		if(profileDoc.getItemValueString("feature_categories")=="Y"){
			return true;
		}else{
			return false;
		}
		
	} catch (e) {
			print (e);			
	}}
		
	//--------------------------------
	this.prototype.doCategoryLookup = function() {
	//--------------------------------	
	
	try {
				
		if(profileDoc.getItemValueString("feature_categories_userdefined")=="Y"){
			return true;
		}else{
			return false;
		}
		
	} catch (e) {
			print (e);			
	}}	
				
	
	//--------------------------------
	this.prototype.getHTMLOption = function() {
	//--------------------------------
	try{
		
	return profileDoc.getItemValueString("allowhtml");
	
	//print("Get:"+profileDoc.getItemValueString("allowhtml"));
	
	} catch (e) {
			print (e);
		}
	}
	
	//--------------------------------
	this.prototype.getWikiUrl = function() {
	//--------------------------------
	try{
	return profileDoc.getItemValueString("server_host");	
	} catch (e) {
			print (e);
		}
	}
	
//--------------------------------
	this.prototype.getTabConfig = function() {
	//--------------------------------
	importPackage(com.ibm.lotus.idc.wiki);
	try{
		var configView:NotesView = database.getView("vConfig");
		var configDoc:NotesDocument = configView.getFirstDocument();
		var dbloc = "/" + @ReplaceSubstring(@Subset(@DbName(), -1),"\\","/");
		var configMap = new Object();
		
		if(configDoc){
			// set header info 
			configMap.head = configDoc.getItemValueString("content");
			configMap.column1 = configDoc.getItemValueString("homecolumn1");
			configMap.column2 = configDoc.getItemValueString("homecolumn2");
			configMap.column3 = configDoc.getItemValueString("homecolumn3");
			configMap.column4 = configDoc.getItemValueString("homecolumn4");
			configMap.column5 = configDoc.getItemValueString("homecolumn5");
			configMap.wikipromo = configDoc.getItemValueString("wikipromo");
			configMap.pdSection1Title = configDoc.getItemValueString("docsection1title");
			configMap.pdSection1Text = configDoc.getItemValueString("docsection1text");
			configMap.pdSection2Title = configDoc.getItemValueString("docsection2title");
			configMap.pdSection2Text = configDoc.getItemValueString("docsection2text");
			configMap.commSection1Title = configDoc.getItemValueString("commsection1title");
			configMap.commSection1Text = configDoc.getItemValueString("commsection1text");
			configMap.commSection2Title = configDoc.getItemValueString("commsection2title");
			configMap.commSection2Text = configDoc.getItemValueString("commsection2text");
			configMap.learnSection1Title = configDoc.getItemValueString("learnsection1title");
			configMap.learnSection1Text = configDoc.getItemValueString("learnsection1text");
			configMap.learnSection2Title = configDoc.getItemValueString("learnsection2title");
			configMap.learnSection2Text = configDoc.getItemValueString("learnsection2text");
			configMap.rbSection1Title = configDoc.getItemValueString("rbsection1title");
			configMap.rbSection1Text = configDoc.getItemValueString("rbsection1text");
			configMap.rbSection2Title = configDoc.getItemValueString("rbsection2title");
			configMap.rbSection2Text = configDoc.getItemValueString("rbsection2text");
					
			// get product doc info
			var pDocNav:java.util.Vector = configDoc.getItemValue("docnav");
			var pdHelper:ConfiguredProducts = new ConfiguredProducts();
			pdHelper.init(pDocNav);
			var jsonList = pdHelper.getProductListings();
			configMap.productDocNav = jsonList;
						
			return configMap;			
		}	
	} catch (e) {
			print (e);
		}
	}
	
	//--------------------------------
	this.prototype.getFilterTCOption = function() {
	//--------------------------------
	try{
		var enableTC = profileDoc.getItemValueString("filteredTagCloud");
		if(enableTC === "Yes"){
			return 	true;		
		}else{
			return false;
		}
	
	} catch (e) {
			print (e);
		}
	}
	
	//--------------------------------
	this.prototype.getDebugSetting = function() {
	//--------------------------------

		try{
			var enableDebug = profileDoc.getItemValueString("enableDebug");
			if(enableDebug === "Yes"){
				return true;
			}else{
				return false;
			}
		}catch(e){
			print(e);
		}
	}
}	


//--------------------------------
function getCategoryHTML(strCategory) {
//--------------------------------
// Get html for this category

var outHTML="";	

var db:NotesDatabase = database;
var view:NotesView=database.getView("vSysCat");
var doc:NotesDocument=view.getDocumentByKey(strCategory,true);

if (doc!=null){

var contType = doc.getItemValueString("contentType");
var catImage = "";
var catAltText = "";

switch(contType){
	
	case "other":
		catImage = "communityblue24.png";
		catAltText = "Community";
		break;
	case "prodDoc":
		catImage = "proddocblue24.png";
		catAltText = "Product Documentation";
		break;
	case "learning":
		catImage = "learningblue24.png";
		catAltText = "Learning";
		break;
	case "books":
		catImage = "booksblue24.png";
		catAltText = "Books"
		break;
	default:
		catImage = "communityblue24.png";
		catAltText = "Community";
		break;
		
}
	outHTML = "<header>"
		outHTML +="<h1 class=\"articleTitle\" style=\"border-bottom:1px solid #ccc;margin-top:1px;\"><img class=\"wikipagetypeicon\" alt=\"\" src=\"" + catImage + "\"/>"+strCategory+"</h1>";
		outHTML +="</header>"
		// MKS 7/14/2011 Candidate fix for IE6 display problem. AJBN8JPPTW
		outHTML += "<div>";
		outHTML += doc.getItemValueString("categoryhtml");
		outHTML += doc.getItemValueString("categoryhtml2");
		outHTML += "</div>";
		
}


return outHTML;

}

//-------------------------------- 
function clearVariables() { 
//-------------------------------- 
// Clear variables from memory 

html=""; 
outHTML=""; 
strHTML=""; 
inHTML=""; 
contentHTML=""; 
x=null; 
} 
