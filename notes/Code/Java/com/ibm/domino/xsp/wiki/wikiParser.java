/**
(c) IBM Corp., 2009 Licensed Materials
 */

/**
Licensed to OpenNTF under one or more contributor license agreements.

See the NOTICE file distributed with this work for additional information
regarding copyright ownership.  OpenNTF licenses this file
to you under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. 

*/

package com.ibm.domino.xsp.wiki;
import java.util.regex.*;
import java.net.URLEncoder;

public class wikiParser {
	
	private static String LINE_BREAK="\r\n";
	private static String LINE_BREAK_MARKER="<linebreak>";
	private static String REGEX_HEADINGS="(?:<p dir\\=\"ltr\">(?:\\s*)|(?:\\s*))(\\={2,4})(.*?)(\\={2,4})";
		
	public static String processHeadings(String strIn){
		StringBuffer sb = new StringBuffer();
		int headingLevel=0;
		String headingName="";
		
		try{			
			Pattern p= Pattern.compile(REGEX_HEADINGS);
			Matcher m= p.matcher(strIn);		    
			
	        boolean result = m.find();
	        // Loop and replace
	        while(result) {
	        	//headingLevel=m.group(3).length();
	        	//headingName=m.group(4);
	        	headingLevel=m.group(1).length();
	        	//System.out.println("Heading Level = " + headingLevel);
	        	headingName=m.group(2);
	        	//System.out.println("Heading Name = " + headingName);
	        	//System.out.println("<a name=\""+URLEncoder.encode(headingName, "UTF-8")+"\"></a><span class=\"docHeading\"><h"+headingLevel+">"+headingName+"</h"+headingLevel+"></span>");
	            m.appendReplacement(sb, "<a name=\""+URLEncoder.encode(headingName, "UTF-8")+"\"></a><span class=\"docHeading\"><h"+headingLevel+">"+headingName+"</h"+headingLevel+"></span>");
	            result = m.find();	            
	        }
	        // complete
	        m.appendTail(sb);			
		}
		catch (Exception e){
			System.out.println(e);
			
		}
		
		return sb.toString();
	}
	
	public static String processLists(String strIn, String strHTML, String strIdent){
		
		StringBuffer sb = new StringBuffer("<"+strHTML+">");
		String[] temp;
		int lastLevel=1;
		int currentLevel=1;		
				
		try{
						
			temp=strIn.split(LINE_BREAK);
			
			for(int x=0;x<temp.length;x++){	
				
				currentLevel=(temp[x].substring(0,3).lastIndexOf(strIdent))+1;
				
				if (strIdent=="*" && temp[x].substring(0,currentLevel-1).lastIndexOf(" ")>=0){
					//if space found then we have a bullet and immediate bold so need to try again
					currentLevel=(temp[x].substring(0,3).indexOf(" "));					
				}				
				
				//sb.append("\n");
				if(currentLevel>lastLevel){
					//New Level
					sb.append("<"+strHTML+">"+LINE_BREAK_MARKER);					
				}
				while(currentLevel<lastLevel){
					//Close Level
					sb.append(LINE_BREAK_MARKER+"</"+strHTML+">");	
					lastLevel--;
				}
		        				
				sb.append("<li>"+temp[x].substring(currentLevel)+"</li>");				
				
				lastLevel=currentLevel;
			}
			
			while(lastLevel>1){
				//Close Level
				sb.append(LINE_BREAK_MARKER+"</"+strHTML+">");	
				lastLevel--;
			}
			sb.append(LINE_BREAK_MARKER);
		}catch (Exception e){
			System.out.println(e);
			
		}		
		
		sb.append(LINE_BREAK+"</"+strHTML+">");
				
		return sb.toString();
	}

}
